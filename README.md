lnch
====

## About

This is a fork of the [`lnch` program by Ömür Özkir](https://github.com/oem/lnch). Some relevant information on it can be found [here](https://medium.com/njiuko/using-fzf-instead-of-dmenu-2780d184753f).
